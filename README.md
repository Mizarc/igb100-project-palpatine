# IGB100 First Person Game Project

It ain't much but it's honest work. Looks like parkour game is on the menu boys.

Created by Kevin Rahardjo & Kai Forman.

## What's completed
- Movement

## Roadmap
- Basic models
- Player mechanics
- Enemies
- Main menu
- Finalised models
- Textures