﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float hoverSpeed;
    public float hoverAmount;
    public float attackDistance;
    private float midpoint = 2f;

    private float timer = 0.0f;
    private float attackTimer = 0.0f;
    private GameObject player;
    private ParticleSystem particles;
    private ParticleSystem attackParticles;
    private bool attacking;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        particles = gameObject.GetComponent<ParticleSystem>();
        particles.Stop();

        GameObject attackObject = gameObject.transform.GetChild(0).gameObject;
        attackParticles = attackObject.GetComponent<ParticleSystem>();
        var em = attackParticles.emission;
        em.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        Hover();
        Look();
        ChargeUp();
        Attack();
    }

    void Hover()
    {
        Vector3 turretPos = transform.localPosition;
        float sineWave = 0f;
        sineWave = Mathf.Sin(timer);
        timer = timer + hoverSpeed;
        if (timer > Mathf.PI * 2)
        {
            timer = timer - (Mathf.PI * 2);
        }

        float translateChange = sineWave * hoverAmount;
        //float totalAxes = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
        //totalAxes = Mathf.Clamp (totalAxes, 0.0f, 1.0f);
        //translateChange = totalAxes * translateChange;
        turretPos.y = midpoint + translateChange;

        transform.localPosition = turretPos;
    }

    void Look()
    {
        transform.LookAt(player.transform, Vector3.up);
        transform.Rotate(0, 180, 0);
    }

    void ChargeUp()
    {
        if (attacking)
        {
            return;
        }

        if (Vector3.Distance(transform.position, player.transform.position) < attackDistance )
        {
            attackTimer += Time.deltaTime;
            if (particles.isStopped)
            {
                particles.Play();
            }
        }
        else if (Vector3.Distance(transform.position, player.transform.position) > attackDistance )
        {
            if (particles.isPlaying)
            {
                attackTimer += 0f;
                particles.Stop();
            }
        }
    }

    void Attack()
    {
        if (attackTimer > 3f)
        {
            if (attackParticles.isStopped)
            {
                var em = attackParticles.emission;
                em.enabled = true;
                attacking = true;
                particles.Stop();
                attackParticles.Play();
                player.GetComponent<PlayerOther>().Death();
            }
        }
        else
        {
            attacking = false;
            attackParticles.Stop();
        }
    }

    void Death()
    {
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collider)
    {
        if(collider.gameObject.tag == "Cubeios")
        {
            Death();
        }
        
    }
}
