﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawning : MonoBehaviour
{
    public GameObject turret;

    // Start is called before the first frame update
    void Start()
    {
        GameObject turret_spawn = Instantiate(turret, transform);
        turret_spawn.transform.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
