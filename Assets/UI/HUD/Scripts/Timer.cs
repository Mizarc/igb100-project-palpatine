﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Diagnostics;

public class Timer : MonoBehaviour
{
    public Text timerLabel;

    public Text endtimer;

    public Text Highscore;

    float currentscore;

    private float time;

    public static string finishedtime;

    public static bool isRunning = false;
    // Start is called before the first frame update
    void Start()
    {
        isRunning = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (IfinEndzone.stoptimer == false)
        {
            time += Time.deltaTime;

            var minutes = time / 60; //Divide the guiTime by sixty to get the minutes.
            var seconds = time % 60;//Use the euclidean division for the seconds.
            var fraction = (time * 100) % 100;

            //update the label value
            timerLabel.text = string.Format("{0:00} : {1:00} : {2:00}", minutes, seconds, fraction);
        }
        if (IfinEndzone.stoptimer == true)
        {
            var minutes = time / 60; //Divide the guiTime by sixty to get the minutes.
            var seconds = time % 60;//Use the euclidean division for the seconds.
            var fraction = (time * 100) % 100;



            endtimer.text = string.Format("{0:00} : {1:00} : {2:00}", minutes, seconds, fraction);



            currentscore = time;

           if (currentscore < PlayerPrefs.GetFloat("HighScore"))
            {
                PlayerPrefs.SetFloat("HighScore", time);
            }

            var minutes2 = PlayerPrefs.GetFloat("HighScore") / 60; //Divide the guiTime by sixty to get the minutes.
            var seconds2 = PlayerPrefs.GetFloat("HighScore") % 60;//Use the euclidean division for the seconds.
            var fraction2 = (PlayerPrefs.GetFloat("HighScore") * 100) % 100;

            Highscore.text = string.Format("{0:00} : {1:00} : {2:00}", minutes2, seconds2, fraction2);

            if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene("Level 2");
                IfinEndzone.stoptimer = false;
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("SampleScene");
                IfinEndzone.stoptimer = false;
            }
        }


    }
}
