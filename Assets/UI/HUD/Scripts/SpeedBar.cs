﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedBar : MonoBehaviour
{
    public Slider slider;

    public void SetSpeed(float speed)
    {
        slider.value = speed;
    }
}
