﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    private GameObject player;
    private bool captured;
    private Vector3 lastPosition;
    private float moveTime;

    void Start()
    {
        moveTime = 0f;
    }

    void Update()
    {
        if (captured)
        {
            Pull(player);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.name == "Player")
        {
            player = collider.gameObject;
            lastPosition = collider.gameObject.transform.position;
            CharacterController control = collider.gameObject.GetComponent<CharacterController>();
            control.enabled = false;
            captured = true;
        }
    }

    private void Pull(GameObject gameObject)
    {
        moveTime += Time.deltaTime / 2f;
        gameObject.transform.position = Vector3.Lerp(lastPosition, transform.position, moveTime);
    }
}
