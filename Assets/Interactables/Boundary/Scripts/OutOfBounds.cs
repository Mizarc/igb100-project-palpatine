﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : MonoBehaviour
{
    private PlayerOther player;

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerOther>();
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.name == "Player")
        {
            player.Death();
        }
        
    }
}
