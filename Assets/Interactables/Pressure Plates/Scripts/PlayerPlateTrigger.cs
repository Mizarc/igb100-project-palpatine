﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlateTrigger : MonoBehaviour
{
    private PressurePlate plate;

    void Start()
    {
        plate = this.GetComponentInParent<PressurePlate>();
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (plate.activationObject == 0 && other.gameObject.tag != "Player")
        {
            return;
        }
        else if (plate.activationObject == 1 && other.gameObject.tag != "Cubeios")
        {
            return;
        }

        plate.isPressed = true; 
    }

    void OnTriggerExit(Collider other)
    {
        if (plate.requiresHeld == true)
        {
            plate.isPressed = false;
        }
    }
}
