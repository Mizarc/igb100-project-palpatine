using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateTrigger : MonoBehaviour
{
    private PressurePlate plate;

    void Start()
    {
        plate = this.GetComponentInParent<PressurePlate>();
    }

    void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.gameObject.name);
        if (plate.activationObject == 0 && other.gameObject.tag != "Player")
        {
            return;
        }
        else if (plate.activationObject == 1 && other.gameObject.tag != "Cubeios")
        {
            return;
        }

        plate.isPressed = true; 
    }

    void OnCollisionExit(Collision other)
    {
        if (plate.requiresHeld == true)
        {
            plate.isPressed = false;
        }
    }
}
