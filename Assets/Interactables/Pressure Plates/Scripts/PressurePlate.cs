﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    public GameObject platform;
    public GameObject platformGhost;
    public GameObject pressurePlate;
    public Material litMaterial;
    public Material unlitMaterial;
    public int activationObject;
    public bool requiresHeld;
    public bool isPressed;
    private float unpressedYPos;
    private float pressedYPos;

    void Start()
    {
        unpressedYPos = pressurePlate.transform.localPosition.y;
        pressedYPos = pressurePlate.transform.localPosition.y - 0.2f;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed)
        {
            PressAnim();
            return;
        }
        DepressAnim();
    }

    private void PressAnim()
    {
        if (pressurePlate.transform.localPosition.y > pressedYPos)
        {
            pressurePlate.transform.Translate(-Vector3.up * Time.deltaTime / 3);
            return;
        }
        MaterialisePlatform();
    }

    private void DepressAnim()
    {
        if (pressurePlate.transform.localPosition.y < unpressedYPos)
        {
            pressurePlate.transform.Translate(Vector3.up * Time.deltaTime / 3);
            return;
        }
        DematerialisePlatform();
    }

    private void MaterialisePlatform()
    {
        
        Renderer g_renderer = platformGhost.GetComponent<Renderer>();
        g_renderer.enabled = false;

        Renderer p_renderer = platform.GetComponent<Renderer>();
        p_renderer.enabled = true;
        Collider p_collider = platform.GetComponent<Collider>();
        p_collider.enabled = true;
        MeshRenderer meshRenderer = pressurePlate.GetComponent<MeshRenderer>();
        meshRenderer.material = litMaterial;
    }

    private void DematerialisePlatform()
    {
        Renderer g_renderer = platformGhost.GetComponent<Renderer>();
        g_renderer.enabled = true;

        Renderer p_renderer = platform.GetComponent<Renderer>();
        p_renderer.enabled = false;
        Collider p_collider = platform.GetComponent<Collider>();
        p_collider.enabled = false;
        MeshRenderer meshRenderer = pressurePlate.GetComponent<MeshRenderer>();
        meshRenderer.material = unlitMaterial;
    }
}
