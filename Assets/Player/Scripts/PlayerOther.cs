﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class PlayerOther : MonoBehaviour
{
    public GameObject overlayGO;

    private Image overlay;
    private bool dead;
    private float fadeoutLerp;
    private float fadeoutDuration = 1f;
    private Color startFade;
    private Color endFade;

    void Start()
    {
        overlay = overlayGO.GetComponent<Image>();
        startFade = new Color(overlay.color.r, overlay.color.g, overlay.color.b, 0f);
        endFade = new Color(overlay.color.r, overlay.color.g, overlay.color.b, 1f);
    }

    void Update()
    {
        FadeOut();
        Reset();
    }

    public void Death()
    {
        overlay.enabled = true;
        dead = true;
    }

    void FadeOut()
    {
        if (!dead)
        {
            return;
        }

        Debug.Log(fadeoutLerp);
        overlay.color = Color.Lerp(startFade, endFade, fadeoutLerp);

        if (fadeoutLerp < 1)
        {
            fadeoutLerp += Time.deltaTime / fadeoutDuration;
        }
    }

    void Reset()
    {
        if (fadeoutLerp >= 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
