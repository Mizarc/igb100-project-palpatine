﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBobbing : MonoBehaviour
{
    public float bobbingSpeed = 0.04f;
    public float bobbingAmount = 0.09f;

    private float midpoint = 1.27f;
    private float timer = 0.0f;
    private CharacterController controller;


    void Start()
    {
        controller = gameObject.GetComponentInParent(typeof(CharacterController)) as CharacterController;
    }

    void Update()
    {
        if (!controller.isGrounded)
        {
            return;
        }

        float sineWave = 0f;
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 cameraPos = transform.localPosition;
  
        // Run a sine wave equation relative to a timer as long as the player
        // is moving
        if (Mathf.Abs(horizontal) != 0 || Mathf.Abs(vertical) != 0)
        {
            sineWave = Mathf.Sin(timer);
            timer = timer + bobbingSpeed;
            if (timer > Mathf.PI * 2)
            {
                timer = timer - (Mathf.PI * 2);
            }
        }
        else
        {
            timer = 0.0f;
        }
        
        // Use the provided sine wave to have the player camera follow the
        // same movement
        if (sineWave != 0)
        {
            float translateChange = sineWave * bobbingAmount;
            float totalAxes = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            totalAxes = Mathf.Clamp (totalAxes, 0.0f, 1.0f);
            translateChange = totalAxes * translateChange;
            cameraPos.y = midpoint + translateChange;
        }
        else 
        {
            cameraPos.y = midpoint;
        }

        transform.localPosition = cameraPos;
    }
}
