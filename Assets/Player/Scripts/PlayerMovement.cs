﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public float speed = 12f;
    public float sprintAcceleration = 0.1f;
    public float jumpHeight = 15f;
    public float gravity = 9.81f;
    public float mass = 50f;
    public SpeedBar speedBarL;
    public SpeedBar speedBarR;
    public float overallSpeed;

    [SerializeField]
    private GameObject arms;

    private Vector3 vSpeed;
    private float walkMultiplier;
    private float sprintMultiplier;
    private float zAirSpeed = 5f;
    private float xAirSpeed = 5f;
    private Vector3 airborneSpeed;
    private bool isAirborne;
    private float justLanded;
    private AnimateArms anims;

    void Start()
    {
        anims = arms.GetComponent<AnimateArms>();
    }

    void Update()
    {
        Move();
        Gravity();
        Jump();
        Sprint();
    }

    /// <summary>
    /// Ensures that the player is given a constant downwards force of a given
    /// gravity relative to their mass. If the player is on the ground, a
    /// constant force of 1f is applied to ensure that 
    /// </summary>
    void Gravity()
    {
        vSpeed.y -= gravity * (mass / 25) * Time.deltaTime;
        controller.Move(vSpeed * Time.deltaTime);

        if (controller.isGrounded)
        {
            vSpeed.y = -1f;
        }
    }

    /// <summary>
    /// Binds the jump button to give the player a quick upwards burst of
    /// upwards acceleration, simulating a jumping motion.
    /// </summary>
    void Jump()
    {
        if (Input.GetButtonDown("Jump") && controller.isGrounded)
        {
            vSpeed.y = jumpHeight;
            controller.Move(vSpeed * Time.deltaTime);
            anims.JumpAnim();
            isAirborne = true;
        }

        if (isAirborne == true && controller.isGrounded)
        {
            isAirborne = false;
            justLanded = 0.1f;
            walkMultiplier = 0.5f;
        }

        if (justLanded > 0)
        {
            justLanded -= Time.deltaTime;
        }
    }
    
    /// <summary>
    /// Uses the axis controls to move the player in the specified direction.
    /// The speed at which the player moves is determined by the set speed
    /// multiplied by the sprint multiplier.
    /// </summary>
    void Move()
    {
        float xMovement = Input.GetAxis("Horizontal");
        float yMovement = Input.GetAxis("Vertical");

        if (xMovement == 0 && yMovement == 0)
        {
            walkMultiplier = 0;
        }

        if (walkMultiplier < 1)
        {
            walkMultiplier += Time.deltaTime;
        }

        if (justLanded > 0.08f)
        {
            walkMultiplier = 0.5f;
        }

        if (controller.isGrounded)
        {
            Vector3 direction = transform.right * xMovement
                + transform.forward * yMovement;
            controller.Move(direction * (speed * walkMultiplier) * sprintMultiplier * Time.deltaTime);
            airborneSpeed = new Vector3(controller.velocity.x * 0.8f, 0f, controller.velocity.z * 0.8f);
        }
        else
        {
            Vector3 direction = transform.right * xMovement * xAirSpeed
                + transform.forward * yMovement * zAirSpeed;
            controller.Move((airborneSpeed + direction) * Time.deltaTime);
        }
    }

    /// <summary>
    /// Binds the left shift key to sprint. This initially modifies the sprint
    /// multiplier to give the player a quick 20% boost, followed by a constant
    /// 10% multiplier increase per second.
    /// </summary>
    void Sprint()
    {
        speedBarL.SetSpeed(sprintMultiplier);
        speedBarR.SetSpeed(sprintMultiplier);

        // Rapidly decelerate when key is released key or player is stopped
        overallSpeed = airborneSpeed.magnitude;
        if (!Input.GetKey(KeyCode.LeftShift) || overallSpeed < 3)
        {
            if (sprintMultiplier > 1)
            {
                sprintMultiplier -= Time.deltaTime * 2f;
                return;
            }
            else
            {
                sprintMultiplier = 1f;
                return;
            }
        }

        if (justLanded > 0)
        {
            sprintMultiplier -= Time.deltaTime;
        }

        // Initial speed boost when the sprint key is hit
        if (sprintMultiplier < 1.2)
        {
            sprintMultiplier = 1.2f;
        }
        // Constantly accelerate until the speed multiplier is at 3
        else if (sprintMultiplier < 2.5)
        {
            sprintMultiplier += sprintAcceleration * Time.deltaTime;
        }
    }
}
