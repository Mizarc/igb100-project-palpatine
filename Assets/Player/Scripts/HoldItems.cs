﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldItems : MonoBehaviour
{
    public GameObject carriedObject;
    
    private GameObject camera;
    private bool isCarrying;
    private float isThrowing = 0;
    private AnimateArms anims;
    private float speed;

    [SerializeField]
    private GameObject arms;
    

    void Start()
    {
        anims = arms.GetComponent<AnimateArms>();
        camera = GameObject.FindWithTag("MainCamera");
    }

    void Update()
    {
        PlayerMovement speedComponent = GetComponent<PlayerMovement>();
        speed = speedComponent.overallSpeed;

        Throwing();
        if(!isCarrying && isThrowing < 0.1)
        {
            Pickup();
            return;
        }
        else if (isThrowing < 0.1)
        {
            Carry(carriedObject);
            Throw();
            DropCheck();
        }
    }

    /// <summary>
    /// Selects the rigidbody with the Moveable script attached that is directly
    /// in the center of the camera's view.
    /// </summary>
    void Pickup()
    {
        if(Input.GetMouseButtonDown(1))
        {
            int x = Screen.width / 2;
            int y = Screen.height / 2;

            Ray ray = camera.GetComponent<Camera>()
                .ScreenPointToRay(new Vector3(x, y));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if(hit.distance > Mathf.Clamp(speed * 2f, 10f, 50f))
                {
                    return;
                }

                Moveable moveable = hit.collider.GetComponent<Moveable>();
                if (moveable != null)
                {
                    carriedObject = moveable.gameObject;
                    Rigidbody carriedRigidbody = carriedObject
                        .GetComponent<Rigidbody>();
                    carriedRigidbody.useGravity = false;
                    carriedRigidbody.velocity = Vector3.zero;
                    isCarrying = true;
                    anims.GrabAnim();
                }
            }
        }
    }

    /// <summary>
    /// Forces a selected rigidbody to stay 7 units in front of the camera
    /// at all times. A lerp is applied to ensure that the object has smooth
    /// movement
    /// </summary>
    /// <param name="carriedObject">
    /// The gameobject to be locked in front of the camera
    /// </param>
    void Carry(GameObject carriedObject)
    {
        Vector3 position = camera.transform.position
            + camera.transform.forward * 5f;
        
        carriedObject.transform.position = Vector3.Lerp(
            carriedObject.transform.position, position, Time.deltaTime * (8f - (speed / 10)));
    }

    /// <summary>
    /// Binds left click to the drop method
    /// </summary>
    void DropCheck()
    {
        if(Input.GetMouseButtonUp(1))
        {
            Drop();
            anims.ReleaseAnim();
        }
    }

    /// <summary>
    /// Drops the currently carrying item and restores its gravity
    /// </summary>
    void Drop()
    {   
        carriedObject.GetComponent<Rigidbody>().useGravity = true;
        carriedObject = null;
        isCarrying = false;
    }

    /// <summary>
    /// Throws the currently carrying item with 100f of force in the direction
    /// that the camera is currently looking at
    /// </summary>
    void Throw()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Rigidbody carriedRigidbody = carriedObject
                .GetComponent<Rigidbody>();
            carriedRigidbody
                .AddForce(camera.transform.forward * Mathf.Clamp(100f * speed / 10, 100f, 500f), ForceMode.Impulse);
            carriedRigidbody.AddTorque(1f, 1f, 1f, ForceMode.Impulse);
            anims.ThrowAnim();

            Drop();
            isThrowing = 0.7f;
        }
    }

    void Throwing()
    {
        if (isThrowing > 0)
        {
            isThrowing -= Time.deltaTime;
        }
    }
}
