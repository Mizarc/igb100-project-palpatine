﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimateArms : MonoBehaviour
{
    public GameObject target;
    private Animator animator;
    private HoldItems heldItem;
    private bool isHolding;
    private bool isThrowing;

    [SerializeField]
    [Range(0f, 1f)]
    private float holdingAnimWeight;

    [SerializeField]
    private GameObject rightShoulder;

    [SerializeField]
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        heldItem = player.GetComponent<HoldItems>();
    }

    // Update is called once per frame
    void Update()
    {
        RunAnim();
        HoldAnimWeight(isHolding);
    }

    void LateUpdate()
    {
        ArmPointing();
    }

    public void JumpAnim()
    {
        animator.SetTrigger("triggerJump");
    }

    public void ThrowAnim()
    {
        animator.SetTrigger("triggerThrow");
        isThrowing = false;
    }

    public void ReleaseAnim()
    {
        animator.SetTrigger("triggerRelease");
        isHolding = false;
    }

    public void GrabAnim()
    {
        
        animator.SetTrigger("triggerGrab");
        isHolding = true;
    }

    public void HoldAnimWeight(bool holdState)
    {
        int holdStateInt = Convert.ToInt32(holdState);
        holdingAnimWeight = Mathf.Lerp(holdingAnimWeight, holdStateInt, Time.deltaTime * 5f);
        animator.SetLayerWeight(1, holdingAnimWeight);
    }

    void RunAnim()
    {
        float yMovement = Input.GetAxis("Vertical");
        if (yMovement != 0)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }
    }
    
    void ArmPointing()
    {
        if (isHolding)
        {

        }
    }
}
