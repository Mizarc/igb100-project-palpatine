﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class IfinEndzone : MonoBehaviour

{
    public static bool stoptimer = false;

    public Text score;

    public GameObject Panel;
    public GameObject Panel2;
    GameObject go = null;
    void Start()
    {
        Panel.gameObject.SetActive(false);
        Panel2.gameObject.SetActive(false);
    }


    private void OnTriggerEnter(Collider collider)
    {
        stoptimer = true;
        Panel.gameObject.SetActive(true);
        Panel2.gameObject.SetActive(true);

        score.text = Timer.finishedtime;
    }
}
